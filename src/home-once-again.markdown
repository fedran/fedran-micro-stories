---
title: Home Once Again
---

The wind blew through his hair, teasing against the feathers he had woven into his braids. It brought the scent of salt and spray. The memories that rode along were a pleasant journey from the five years ago when he left.

He got a chance to go out and see the world. He did. He saw, he explored, and now he and his new family were coming home to stay.

A new smell rose up, painfully familiar but one that shouldn't be there. Fire, smoke, and blood.

Sweat on his brow, he hurried the horses.
