---
title: Sharpening Duties
date: 2019-09-29
---

> A man plays no riskier game than leaving a mother childless. --- Niser Higuin-Pos

Karin sat among the freshly cooked pies sharpening the sword of a murderer. The smells of apples and cinnamon where strong as was the stench of blood that still clung to the blade. It had been cleaned before it was presented to her but Karin could still tell.

Her brow furrowed in concentration as she stroked the metal with delicate strokes that moved from the center to the edge. Underneath her thumb, the nearly invisible nicks and bent metal were painfully obvious.

With each movement, the metal flowed minutely. It melted into the nicks and made them strong. It evened out the warped and bent edges into a straight line.

"Can you make it sharper?" The killer was an older woman well into her fifties. She wore a black dress and had a widower's blade. Both the fabric and her face looked worn and exhausted.

She glanced up at the murderer and shrugged. "This is an old blade, you don't have much more to work with."

"Just a few more and I'm done. Please? It has to be this weapon."

Karin didn't quite understand but she could tell there was passion in the woman's eyes. She nodded and continued to work. When she found a stress fracture, she almost gave up. One good twist and the entire thing would twist.

She stopped, considering her actions. She didn't know who was being killed nor the reason.

"Something wrong?" There was a tension in the old woman's voice. Her arm shifted slightly.

Karin glanced up to see her reaching for another knife among the apple peels. Her heart beat faster for a moment. Then she looked up and shook her head. "No, I found a crack and I need to fix it."

"Oh."

At the sight of desperate relief, Karin knew she had to keep going. She furrowed her brow to concentrate and focused on working the metal into the cracks, filling them in and strengthening them. She didn't know how many more lives the blade needed to take but she was going to make sure it would last longer than the lives the old woman was hunting.

She finished with a sigh of exhaustion. Using magic always sapped her strength. Trembling, she set it down next to the old worn sheath of a Kormar infantry. "Done. Be careful, it will cut through almost anything."

"Good."

Karin wiped her brow. "Anything else?"

"No, I have your pies already wrapped up. A dozen for the Rats." The old woman held up a wide basket still steaming from the freshly made pies insides. The smell of cinnamon and surge rose up and Karin's stomach rumbled.

Karin pushed herself up from her seat. "Thank you."

Before Karin realized she was moving, the old woman was standing next to her and pushing something into her hand. For a moment, she was sure it was a knife but the clink of coins wrapped in fabric stopped her.

She hefted it, there felt like a lot of money in there. She frowned. "What---?"

"No, thank you," interrupted the murderous old woman.
