---
title: A Missed Chance for Royalty
date: 2019-10-14
---

> No sweeter meal than the celebrations that come at the end of harvest, when the food is hot and the winds are cool. --- *A Rose in Fall* (Act 1, Scene 3)

Halis cradled the bun with one hand as she slipped the steaming sausage along the center. With a practiced movement, she set down her tongs and picked up the brush from the bottle of spiced jelly. Two quick swipes left a pair of parallel lines along the meat. She followed with a pinch of brown sugar and a pat of butter. She set the loaded bun on the grill to toast for a few seconds. The heat from the fire rune underneath the metal scraping at her fingers before she could pull herself free.

With a smile, she looked up at Meram and her heart beat a little faster. "A-Anything else?" She blushed at her stuttering.

Meram pushed her long brown hair away from her face and hooked it on her ear. Her eyes scanned the table before she pointed to the jar of crystallized honey sticks. "Two of those, please?"

Halis didn't hear, she was losing herself Meram's smile. She wanted to reach over to pull the woman close, to taste brown sugar on her lips.

"Halis?"

With a blush, Halis jumped and blushed hotly. She fought her urge to stare at Meram again. "What?"

With a grin, Meram gestured to the jar of crystallized honey on bread sticks. "Two of those?"

"Oh, sorry!" Halis grabbed a piece of waxed paper and pulled out two of the sticks. She used her thumb to fold the paper before setting it down on a wooden plate. The toasted bun slid onto the plate next to the sticks. Taking a deep breath to enjoy the sweet smell of sugar, she handed the plate over. "Here you go."

"Thank you."

"You have a good...." Halis's voice trailed off. Meram was already walking away.

"At least you didn't drop the plate this time," Halis's mother stepped up with a metal tin with more sausages for the grill.

Halis blushed even hotter. "Mama."

"You've been pining after her ever since.... what? I remember the harvest festival where you tried to give your crown to her."

"I was six, Mama."

"And now you are eighteen. Not much has changed. I'm sure if I found a crown, you'd still try to crown her."

Halis watched Meram as her crush walked through the crowd. She was alone, that year. Meram's husband had died over a year ago when plague had swept through town. Ever since, there were more than a few men circling the beautiful woman and Halis didn't dare speak up.

Her mother bumped her with her hip. "You could ask. I'm sure I can grill the meat for a night, I've only been doing this for twenty years."

The canvas behind them rustled and then parted as Halis's father and younger brother came through. He was carrying a box of breads.

"Why would you be grilling, Kim? That's Hal's job this year."

Halis's mother shook her head. "Hal has a crush."

"Who?" asked Purim, bouncing up to try peering over the table. He was only five years old and barely spoke without a question. "Is he going to be good for her?"

Halis shook her head. "It doesn't matter, she's...." Her voice trailed off as she saw a young man approach Meram. It was Rail, this year's Harvest King. He was broad-shouldered with tousled black hair, a slightly crooked nose from a brawl, and a smile that won the hearts of a number of the ladies in town.

"She's.... oh," said her father. "Meram again."

Halis turned. "Papa!"

"That's a girl's name!" pipped Purim from below.

Halis's mother patted him on the head. "Good boy. Now shut up and help your father."

"Okay, Mama." The young boy dove back under the canvas, stepping on the edge and causing the wooden structure to sway.

"Don't step on the side!" roared her father before stomping after him.

Halis sighed as she watched Meram talk to Rail for a moment. Then, a bit of hope rose up as they parted ways. She gulped and looked around, sweat prickling her brow. She wanted to say something, to at least speak up.

"Go on," her mother said with a smile.

Wiping her hands on her apron, Halis stumbled around the table and then dove after Meram. She didn't know what she was going to say, she didn't even have an idea why she finally had the courage to speak up.

Throat dry, she pushed her way to Meram who was standing next to one of the game tables. A bunch of kids were fooling around as they tried to throw rings onto tightly packed bottles. They kept stepping out, blocking Halis.

She wanted to scream out for them to move. Instead, she whimpered as she worked her way around. "Mer---"

Rail was back and holding hands with Meram. His head was close to hers as he whispered.

Halis saw the sweet smile on Meram's face, the way her eyes sparkled and her back arched slightly. It was the same thing that Halis had dreamed about for years and, once again, she didn't have the courage to keep going.

Meram glanced toward her and then she turned to face Halis.

Halis's cheeks burned with humiliation. She held up her hands and backed away, staring in embarrassment until the crowd streamed back and broke the eye contact. Turning around, she sighed and trudged back to the table.

Her mother looked at her and held out her arm. "Come here."

Halis hugged her mother for a moment before taking the tongs. "I got this."

"Are you sure?"

"Yes, Mama." It didn't feel like that when she focused on turning the sausages over. Someone would be coming over soon to order more food. It just wasn't the person Meram hoped for.

Three hours later, the sting of regret had faded but not gone away. Everything smelled like sausages and sugar. Halis handed over a bag with five sweet sausages wrapped inside. It was for the trip home for a family heading back in the dark instead of staying in town. "Have a good evening!" she said with a false smile.

Her father peered over the grill. "Five more? It was a good night. Think we can sell the last two before everyone crashes?"

It was almost midnight and the din of the festival had faded into pools of drunken laughter and merriment. The cold wind blew down the deserted lane between the games and the food vendors. Across the way, the couple running the bottle game were draping a canvas over their stall just in case it rained.

"Maybe, Papa."

"Well, keep them running until the herald comes around. You never know."

Halis nodded and rolled the sausages over. There wasn't much left on the table, only a few bits of jelly and maybe a pinch of brown sugar left on the bottom of the jar.

"Hal! Hal!" Purim ran up with something in his hand. "I made you something!"

Halis smiled at her younger brother. When he approached, she crouched down to bring herself down to his height. "What do you have, Pur?"

"I made you a crown!" He brandished a circle of corn stalks and flowers, loosely bundled into a ring with a large mushroom in the front. It was dripping wet and barely held together with a few threads she suspected he ripped out of his jacket.

"It's beautiful," she said.

"You said you wanted a crown, I heard it. So I made you one!"

She hugged him tightly. "You are a good brother."

"Ew! No hugs!" He set down the makeshift crown on the table. Taking a few steps, he suddenly spun around and then ran to hug her tightly. "Love you!"

Halis sighed with a smile. She would always have her family.

"Could I get two sausages to go?"

Halis froze at the sound of Meram's voice. Trembling, she peeked up past Purim's crown to see Meram on the other side of the table.

Her crush's hair was tousled and bunched up. It looked like Meram had tried to brush it out with her fingers but Halis could see a few leaves still stuck in the brown strands. Her smile was still brilliant and she had a faint blush to her cheeks, probably for reasons more than the cool wind.

Gulping, Halis stood up. "Sure... I'll get those for you."

Her hands shook as picked up the buns and set the last two sausages along them. Her heart felt heavy as she worked as much jelly as she could on the brush.

"Halis?"

She froze, her hand shaking.

"I'm sorry." Meram said softly. "I didn't realize you were fishing for Rail, not until... well, I'm sorry if you lost your chance. I'm not interested in... him anymore. Well, I never really was but...." She looked bashful. "Sorry. I should have given you the chance."

Halis gulped and her tongue felt twisted. She tried to speak but the words didn't quite come out. "I wasn't looking for him."

"Oh." Meram's smile lit up the night. "I just thought. I mean, you've always been so sweet to me and I wouldn't want anything between us."

Halis almost burst into tears. She looked down with her blurry vision and fumbled with the brown sugar. It took her three tries before she finally gave up and then just poured the rest over the buns. She burned her fingers on the grill setting them down.

Her chest ached as she looked up. "A-Anything else?"

"Any more of those honey sticks?"

Halis shook her head.

A few moments later and she was handing the paper-wrapped sausages over to her crush.

Meram smiled broadly. "I'll see you tomorrow, Halis? Bring some of the honey sticks? I'll buy a bunch for the trip home."

Halis nodded. "Have a good... evening."

She fought her emotions as she watched Meram walk away. To distract herself, she reached out and grabbed the crown that her brother had made. It was wet but it reminded her of the time she tried to crown Meram.

Her gaze focused on Meram as the brown-haired woman crossed the aisle while looking in the bag.

Then Meram stopped.

Halis inhaled sharply.

Slowly, Meram turned to look over her shoulder. There was a strange look on her face, confusion maybe.

Halis realized she was staring. With a blush, she ducked behind the table in a pretense of picking up the crown. Her cheeks burned with humiliation as she used the bottom of her skirt to mop up the moisture from the woven stalks. Fortunately it was just water, not anything else.

When she finally caught her breath and the burn of her cheeks faded, she took a deep breath and stood up.

Meram was standing on the other side of the table.

Halis gasped.

"Were you looking for me?" Meram asked with a smile.

Halis panicked. She opened her mouth to say something but no words would come out. The seconds passed by and she found herself growing more and more embarrassed. In desperation, she shoved the crown toward Meram. "T-This is for you!"

Meram's smile lit up the night again. She took it and look at it.

"I-I...."

"Remember when we were younger? You shoved a crown like this in my throat? I couldn't talk for days after that."

Halis cringed. "I... I was trying to crown...." Her voice trailed off as Meram lifted the crown and set it down on her tousled hair. Her heart pounded in her chest.

"I think it's beautiful." Meram looked up as if she could see it. "I always wanted to be royalty, even for a night."

The world spun and Halis had to grab the side of the table.

Meram's eyes sparkled as she grinned back at her. "What are you doing tomorrow night?"

"I-I have to work."

Someone kicked her from behind. The canvas fluttered against her back.

Almost dying of embarrassment, Halis cleared her throat. "I can take---"

Another kick. "The night. Take the night," whispered her mother sharply.

There was a giggle, Purim was back there also.

"I have the night off," finished Halis.

Meram smiled and her shoulder shook for a moment. Then she tugged her hair back over her hear. "Good, because I think I've been crowned a Harvest King and I'm pretty sure I need a queen for the night."
