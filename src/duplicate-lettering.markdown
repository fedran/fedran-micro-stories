---
title: Duplicate Lettering
---

Paladam hummed a popular song under his breath as he carefully wrote out a neat line of text. His fingers guided the quill with practiced grace.

Next to him, five other books were spread out. The pages fluttered as the same lettering appeared on them. Flecks of magical ink left faint clouds to dust the page as he wrote line after line.

He didn't mind the solitude or the quiet, it was an appropriate way to pass the time of his sentence.
