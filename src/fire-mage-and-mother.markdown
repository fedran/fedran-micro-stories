---
title: Fire Mage and Mother
---

She had never felt as helpless as she did in that moment, holding her wheezing baby as he clutched to her breast. For all of her abilities to control fire, nothing could get air into his lungs or keep the cough from rattling his tiny lungs. The thousands she killed in battle were nothing to each and every breath.

With tears in her eyes, she clutched him tightly and prayed the night would pass and he would still be there in the morning.
