---
title: Fighting Lessons
---

Larim landed on the ground. She whipped around and tried to concentrate.

Her sparring partner, the famous Naggergan Thut, had crossed the fighting ring in only seconds. Her curved blades flashed as she swung for Larim's throat.

Larim fell back.

"Don't dodge!"

Thut's body blurred as she covered the distance. "Focus!"

Larim gasped and tried to remember how the magic felt. Her body tingled.

The blade slammed into Larim's throat, parting through her now liquid body to hit the ground.
