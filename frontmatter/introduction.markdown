---
title: Micro Stories and Flash Fiction
---

Occasionally, it is fun to write a little drabble or flash fiction about some aspects of Fedran. These are a collection of those stories. They are canon in that they happen in the story but they are not as tracked by point-of-view and volume as strictly as most of the other site.
